#' This function is made to edit taxonomy (`phyloseq::tax_table`) in order to
#' make sure it is compatible with combination of markers.
#' It replaces non-informative taxa names, matchting `unassigned.pattern`, by NAs.
#' It also replaces NAs within taxonomy to "UnkownTaxon"
#' (ex:"Tenericutes|NA|Mycoplasmoidales" becomes "Tenericutes|UnkownTaxon|Mycoplasmoidales").
#' @title Clear taxonomy
#' @param taxonomy A `taxonomyTable` class object (from `phyloseq` package).
#' @param unassigned.pattern A string containing patterns that will be replaced by NAs.
#' Can contain regular expressions in the taxonomy (ex :
#' "Multi-affiliation|None|Uncultured")(it will be given as pattern argument in
#' `stringr::str_detect()`)
#' @return A cleared `taxonomyTable` class object (from `phyloseq` package).
#' @importFrom magrittr %>%
#' @export
#'

clear_taxonomy = function(taxonomy, unassigned.pattern=NA){
  if(!methods::is(taxonomy,'taxonomyTable')){
    stop("Require a taxonomyTable object from phyloseq package")
  }
  # replace empty values by NA
  taxonomy.out = taxonomy %>%
    as.data.frame() %>%
    dplyr::mutate_all(~replace(. , .=="",NA))

  # replace taxa matching unassigned.pattern by NA
  if(!is.na(unassigned.pattern)){
    taxonomy.out = taxonomy.out %>%
      dplyr::mutate_all(~replace(. ,
                                 stringr::str_detect(string = .,
                                                     pattern = unassigned.pattern),
                                 NA))
  }

  # replace insiders NAs by "UnkownTaxon"
  taxonomy.out = suppressWarnings(taxonomy.out %>%
    dplyr::mutate(max.assignation = apply(.,
                                          1,
                                          function(x) max(which(!is.na(x)))))
  )
  for (j in 2:(ncol(taxonomy.out)-2)){
    taxonomy.out = taxonomy.out %>%
      dplyr::mutate_at(j, ~replace(.,is.na(.) & max.assignation>j, "UnkownTaxon"))
  }

  # remove  max.assignation and convert to taxonomyTable class object
  taxonomy.out %>%
    dplyr::select(-max.assignation) %>%
    as.matrix() %>%
    phyloseq::tax_table()
}

